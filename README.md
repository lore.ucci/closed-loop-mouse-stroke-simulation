# Closed loop mouse stroke simulation

This repository contains data and code that pertains to the simulation of a post-stroke robotic rehabilitation experiment performed on mice.

The repository has the following structure:

* data folder: contains the neurophysiological recordings, in mat format
* data_prepared: contains processed information, extracted from the recording files, that can be used as input to the simulation
* src folder: contains the source code for the simulation, implemented as an experiment in the Neurorobotics Platform

## Neurophysiological recordings

Data was recorded in mice during experiment with the M-Platform. In the device, animals were head fixed to the platform with their left wrist constrained to an handle integral with a 1 DOF slide, while a 16 channels linear probe (1 MΩ, ATLAS, Belgium) was inserted into the CFA at 850 µm of depth. Each trial consisted of two phases: first the slide was brought to a target position by a linear actuator to extend the forelimb of the mouse; then the linear actuator was quickly retracted and the animal had to pull the slide to the home position voluntarily. 
    During the experiment, the force signal was acquired by a load cell (Futek LSB200, CA, USA) in the direction of the movement of the slide at 100 Hz, at the same time a webcam recorded the position of the slide at 25 Hz and the electrophysiological signal was recorded by Omniplex D System (Plexon, USA) with a frequency of 40 kHz.
Data of two animals have been recorded in two consecutive days.

The structure of the mat files is as follows:

* file: Name of the file
* FileNamePl2: name of the recording files in format pl2
* fs_cinematica: sample frequency of the data recorded by the robot
* fx_sync: force signal along the main direction synchronized with the electrophysiological data
* index_attempts: index of the onset of the attempts, that are force peaks that doesn't generate a movement of the slide
* index_baseline: central index of an interval of one second where are not recorded force peaks;
* index_mov: index of the onset of the force peaks that generate movements
* pos_sync: position of the slide synchronized with the electrophysiological data
* start: trigger of the beginning of the recordings with the robot in seconds; this trigger is used to generate the synchronized data
* status3: index where the animal is in the active phase and has to pull the slide;
* time_sync: time vector of the robot data in seconds
* trial_sync: number of the trial performed by the animal along time, sampled at 100 Hz, synchronized with the electrophysiological data
* ts(1-16): timestamp of the multiunits for the channel specified by the number; the electrophysiological data are recorded by a 16ch linear probe, where the channel one is the most superficial, while the channel 16th is the deepest one
* wave(1-16): interval of one second of the signal of the correspondent channel for every identified channel

## Simulation software

The simulation is built with the Neurorobotics Platform (NRP), thus this software must be installed (see [NRP website](https://neurorobotics.net/) for instructions).

Another requisite is the installation of [these](https://gitlab.com/sssa-humanoid-robotics/NeuralModels) custom NEST modules and python libraries, that are used to create the spinal cord spiking neural networks.

Once the requisites have been satisfied, the experiment should be put in the local storage of the NRP, and can be run either through the frontend or via the Virtual Coach script provided.