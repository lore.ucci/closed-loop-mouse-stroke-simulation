# Imported Python Transfer Function
from std_msgs.msg import Float64
from rospy import ServiceProxy, wait_for_service, Duration
import rospy


@nrp.MapRobotPublisher('activateRadius1', Topic('/gazebo_muscle_interface/robot/Radius1/cmd_activation', Float64))
@nrp.MapRobotPublisher('activateRadius2', Topic('/gazebo_muscle_interface/robot/Radius2/cmd_activation', Float64))
@nrp.MapRobotPublisher('activateHumerus1', Topic('/gazebo_muscle_interface/robot/Humerus1/cmd_activation', Float64))
@nrp.MapRobotPublisher('activateHumerus2', Topic('/gazebo_muscle_interface/robot/Humerus2/cmd_activation', Float64))
@nrp.MapVariable("spinal_cord", initial_value=hbp_nrp_cle.tf_framework.config.brain_root.network)
@nrp.Neuron2Robot()
def muscle_activation(t, activateRadius1, activateRadius2, activateHumerus1, activateHumerus2, spinal_cord):
    import nest
    import rospy

    if rospy.get_param('sled_moving'):
        humerus1 = 0.0
        humerus2 = 0.0
        radius1 = 0.0
        radius2 = 0.0
    else:
        humerus1 = nest.GetStatus(spinal_cord.value['humerus1_m'], {'activation'})[0][0]
        humerus2 = nest.GetStatus(spinal_cord.value['humerus2_m'], {'activation'})[0][0]
        radius1 = nest.GetStatus(spinal_cord.value['radius1_m'], {'activation'})[0][0]
        radius2 = nest.GetStatus(spinal_cord.value['radius2_m'], {'activation'})[0][0]

    if (not rospy.get_param('can_move')) and (humerus2 > 0.8 or radius1 > 0.8):
        rospy.set_param('can_move', True)

    # acutally release the sled if there is some muscle activation
    if rospy.get_param('can_move'):
        activateHumerus1.send_message(humerus1*0.125*1.5)
        activateHumerus2.send_message(humerus2*0.125*1.5)
        activateRadius1.send_message(radius1*0.125*1.5)
        activateRadius2.send_message(radius2*0.125*1.5)
