# -*- coding: utf-8 -*-
"""
This file contains the spinal cord circuitry
"""
# pragma: no cover

__author__ = 'Lorenzo Vannucci'

import logging
import numpy as np
import nest

from nest_networks.spinalcord import SpinalCord
from muscles_mouse import cord_flags, humerus1, humerus2, radius1, radius2, foot1, foot2, \
                g_humerus1, g_humerus2, g_radius1, g_radius2, g_foot1, g_foot2, \
                flex_extend_humerus, flex_extend_radius, flex_extend_foot

from nest_networks.spikes import import_spikes_from_file, scale_times, trim_to_resolution, remap_gids, get_gids
from nest_networks.recordingplayer import RecordingPlayer

import rospy

logger = logging.getLogger(__name__)

try:
    spikes_file = rospy.get_param('spikes_file')
except KeyError:
    print "No file supplied, reverting to default"
    spikes_file = 'closed-loop-mouse-stroke-simulation/data_prepared/File1_DAY00_Mouse1_sledtimes.txt'

spikes_mu = import_spikes_from_file(spikes_file, 1)
spikes_mu = scale_times(spikes_mu, 1000.0)
spikes_mu = trim_to_resolution(spikes_mu)
gids = get_gids(spikes_mu)
spikes_mu = remap_gids(spikes_mu, orig=gids, dest=range(len(gids)))
rp_mu = RecordingPlayer(spikes_mu, mult=180, sigma=5.)
        
propriosp_hum = nest.Create('iaf_psc_alpha', 196)
propriosp_rad = nest.Create('iaf_psc_alpha', 196)
nest.Connect(rp_mu.get_gids(), propriosp_hum)
nest.Connect(rp_mu.get_gids(), propriosp_rad)

sc = SpinalCord(cord_flags,
                [humerus1, humerus2, radius1, radius2],
                [g_humerus1, g_humerus2, g_radius1, g_radius2],
                [flex_extend_humerus, flex_extend_radius])

network = sc.get_IO_populations()

nest.Connect(network['humerus2_Ia'], propriosp_hum, 'all_to_all', {'weight': -1.0, 'delay': 0.1})
nest.Connect(network['humerus2_II'], propriosp_hum, 'all_to_all', {'weight': -1.0, 'delay': 0.1})
nest.Connect(network['radius1_Ia'], propriosp_rad, 'all_to_all', {'weight': -1.0, 'delay': 0.1})
nest.Connect(network['radius1_II'], propriosp_rad, 'all_to_all', {'weight': -1.0, 'delay': 0.1})

desc = {
        'humerus2': {'alpha': propriosp_hum},
        'radius1': {'alpha': propriosp_rad}
        }

sc.attach_descending(desc)
