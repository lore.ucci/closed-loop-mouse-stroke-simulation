from gazebo_ros_muscle_interface.msg import MuscleStates


@nrp.MapVariable("spinal_cord", initial_value=hbp_nrp_cle.tf_framework.config.brain_root.network)
@nrp.MapRobotSubscriber('muscle_states_msg', Topic('/gazebo_muscle_interface/robot/muscle_states', MuscleStates))
@nrp.Robot2Neuron()
def send_muscle_lengths(t, spinal_cord, muscle_states_msg):

    import nest

    muscle_states = dict((m.name, m) for m in muscle_states_msg.value.muscles)

    # Obtain from osim file.
    l_REF_HUMERUS1 = 0.012200
    l_REF_HUMERUS2 = 0.010242
    l_REF_RADIUS1 = 0.006026
    l_REF_RADIUS2 = 0.005879

    # send values to spindle models

    # Ia
    nest.SetStatus(spinal_cord.value['humerus1_Ia'], {'L': muscle_states['Humerus1'].length / l_REF_HUMERUS1,
                                                      'dL': muscle_states['Humerus1'].lengthening_speed / l_REF_HUMERUS1})
    nest.SetStatus(spinal_cord.value['humerus2_Ia'], {'L': muscle_states['Humerus2'].length / l_REF_HUMERUS2,
                                                      'dL': muscle_states['Humerus2'].lengthening_speed / l_REF_HUMERUS2})
    nest.SetStatus(spinal_cord.value['radius1_Ia'], {'L': muscle_states['Radius1'].length / l_REF_RADIUS1,
                                                     'dL': muscle_states['Radius1'].lengthening_speed / l_REF_RADIUS1})
    nest.SetStatus(spinal_cord.value['radius2_Ia'], {'L': muscle_states['Radius2'].length / l_REF_RADIUS2,
                                                     'dL': muscle_states['Radius2'].lengthening_speed / l_REF_RADIUS2})

    # II
    nest.SetStatus(spinal_cord.value['humerus1_II'], {'L': muscle_states['Humerus1'].length / l_REF_HUMERUS1,
                                                      'dL': muscle_states['Humerus1'].lengthening_speed / l_REF_HUMERUS1})
    nest.SetStatus(spinal_cord.value['humerus2_II'], {'L': muscle_states['Humerus2'].length / l_REF_HUMERUS2,
                                                      'dL': muscle_states['Humerus2'].lengthening_speed / l_REF_HUMERUS2})
    nest.SetStatus(spinal_cord.value['radius1_II'], {'L': muscle_states['Radius1'].length / l_REF_RADIUS1,
                                                     'dL': muscle_states['Radius1'].lengthening_speed / l_REF_RADIUS1})
    nest.SetStatus(spinal_cord.value['radius2_II'], {'L': muscle_states['Radius2'].length / l_REF_RADIUS2,
                                                     'dL': muscle_states['Radius2'].lengthening_speed / l_REF_RADIUS2})