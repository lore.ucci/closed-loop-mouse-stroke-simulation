import sys
import os
sys.path.append(os.path.join(os.environ['HBP'], 'Models', 'cdp1_mouse_w_sled'))
import manualcontrol
reload(manualcontrol)


@nrp.MapVariable('sled_control', initial_value=manualcontrol.SledControl('robot', 'cdp1_msled::world_sled'))
@nrp.MapVariable('spinal_cord', initial_value=hbp_nrp_cle.tf_framework.config.brain_root.network)
@nrp.MapCSVRecorder('recorder', filename='results.csv', headers=['time', 'sledpos', 'humerus1', 'humerus2', 'radius1', 'radius2'])
@nrp.Robot2Neuron()
def monitor_everything(t, sled_control, spinal_cord, recorder):

    # sled position
    sled_pos = sled_control.value.get_last_sled_position()

    # muscle activations
    import nest
    humerus1 = nest.GetStatus(spinal_cord.value['humerus1_m'], {'activation'})[0][0]
    humerus2 = nest.GetStatus(spinal_cord.value['humerus2_m'], {'activation'})[0][0]
    radius1 = nest.GetStatus(spinal_cord.value['radius1_m'], {'activation'})[0][0]
    radius2 = nest.GetStatus(spinal_cord.value['radius2_m'], {'activation'})[0][0]

    recorder.record_entry(t, sled_pos, humerus1, humerus2, radius1, radius2)
