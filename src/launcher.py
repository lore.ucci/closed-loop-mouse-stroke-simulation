from hbp_nrp_virtual_coach.virtual_coach import VirtualCoach
import time
import rospy
import os
import math
from os import listdir
from os.path import isfile, join
import shutil

total_time = 0.0


class Trial:

    def __init__(self, configpath, coach):

        # read parameters from config file and send them through rosparam
        self._configpath = configpath
        self._simtime = self._get_sim_time()
        sled_times = self._get_sled_times()
        print self._simtime
        global total_time
        total_time += self._simtime
        rospy.set_param('sled_times', sled_times)
        rospy.set_param('spikes_file', configpath + '_spikes.txt')

        self._sim = coach.launch_experiment('cdp1_mouse_0')

        self._sim.register_status_callback(self._on_status)

        self._running = False

        self._data_folder = None

    def _on_status(self, msg):
        print msg['simulationTime']
        if msg['simulationTime'] >= self._simtime:
            self._running = False

    def runandwait(self):
        self._running = True
        self._sim.start()
        while self._running:
            time.sleep(0.02)

    def saveandclose(self):
        self._sim.pause()
        self._sim.save_csv()
        self._sim.stop()

    def _get_sled_times(self):
        with open(self._configpath + "_sledtimes.txt") as f:
            content = f.readlines()
        return [float(x.strip()) for x in content]

    def _get_sim_time(self):
        spikefile = open(self._configpath + '_spikes.txt', 'r')
        spikefile.seek(-40, 2)
        line = spikefile.readlines()[-1]
        return math.ceil(float(line.split(' ')[1]))

    def save_meta(self):

        search_dir = "."
        os.chdir(search_dir)
        files = filter(os.path.isdir, os.listdir(search_dir))
        files = filter(lambda x: 'csv_records' in x, files)
        files.sort(key=lambda x: os.path.getmtime(x))
        self._data_folder = files[-1]

        f = open(self._data_folder + '/meta.txt', 'w')
        f.write('datasource = ' + self._configpath)

    def move_data_folder(self, whereto):

        shutil.move(self._data_folder, whereto)


vc = VirtualCoach(environment='local', storage_username='nrpuser', storage_password='password')

# find all data
healthy_path = 'closed-loop-mouse-stroke-simulation/data'
healthy_data = [healthy_path + '_prepared/' + f for f in listdir(healthy_path) if isfile(join(healthy_path, f))]

all_data = healthy_data[0:5]

# launch all
data_count = 1
for _data in all_data:

    # get folder/names
    data = _data.split('.')[0]
    newfolder = 'results/' + _data.split('/')[-2] + '/' + _data.split('/')[-1]
    newfolder = newfolder.split('.')[0]
    newfolder = newfolder.replace('_prepared', '')

    print "%d/%d" % (data_count, len(all_data))
    print data

    # check if folder already exists
    if os.path.exists(newfolder):
        print "folder already exists, skipping"
    else:
        trial = Trial(data, vc)
        trial.runandwait()
        trial.saveandclose()
        trial.save_meta()
        trial.move_data_folder(newfolder)
        time.sleep(10.0)

    print "%d/%d done\n\n" % (data_count, len(all_data))
    data_count += 1

print "Total time: " + str(total_time)
