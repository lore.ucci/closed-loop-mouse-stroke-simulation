cord_flags = {

    'excitatory_II': True,
    'homonymus': True,
    'heteronymus': False,
    'polysynaptic': True,

    'scale': 1,

    # poisson generators for descending inputs
    'descending': False

}

# spindle parameters
spindle_params_Ia = {
    'bag1_G': 20000.0,
    'bag2_G': 10000.0,
    'chain_G': 10000.0
}

spindle_params_II = {
    'bag1_G': 20000.0,
    'bag2_G': 10000.0,
    'chain_G': 10000.0,
    'primary': False
}

biceps_params = {
    'dmn': 8.5,
    'dmx': 120.0,
    'DSF': 1e-6/120.0*109.37,
    'cspf': 1e-2,
    'taumax': 12.5e-3,
    'tauadj': 60e-6,
    'tauslp': 9.44e1,
    'pmx': 15.0,
    'pmn': 2.25,
    'FSF': 1.0/15.0*18.19,
    'smn': 0.104,
    'ssl': 0.207,
    'TSF': 0.6892,
    'Ncal': 740
}


# change these!!!
brachialis_params = {
    'dmn': 8.5,
    'dmx': 120.0,
    'DSF': 1e-6/120.0*109.37,
    'cspf': 1e-2,
    'taumax': 12.5e-3,
    'tauadj': 60e-6,
    'tauslp': 9.44e1,
    'pmx': 15.0,
    'pmn': 2.25,
    'FSF': 1.0/15.0*18.19,
    'smn': 0.104,
    'ssl': 0.207,
    'TSF': 0.6892,
    'Ncal': 740
}


# change these!!!!
triceps_params = {
    'dmn': 8.5,
    'dmx': 120.0,
    'DSF': 1e-6/120.0*109.37,
    'cspf': 1e-2,
    'taumax': 12.5e-3,
    'tauadj': 60e-6,
    'tauslp': 9.44e1,
    'pmx': 15.0,
    'pmn': 2.25,
    'FSF': 1.0/15.0*18.19,
    'smn': 0.104,
    'ssl': 0.207,
    'TSF': 0.6892,
    'Ncal': 740
}


humerus1 = {
    'name': 'humerus1',
    'mn_params': biceps_params,
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'motoneurons': 169,
    'spindles': 60
}

humerus2 = {
    'name': 'humerus2',
    'mn_params': biceps_params,
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'motoneurons': 169,
    'spindles': 60
}

radius1 = {
    'name': 'radius1',
    'mn_params': biceps_params,
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'motoneurons': 169,
    'spindles': 60
}

radius2 = {
    'name': 'radius2',
    'mn_params': biceps_params,
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'motoneurons': 169,
    'spindles': 60
}

foot1 = {
    'name': 'foot1',
    'mn_params': biceps_params,
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'motoneurons': 169,
    'spindles': 60
}

foot2 = {
    'name': 'foot2',
    'mn_params': biceps_params,
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'motoneurons': 169,
    'spindles': 60
}

g_humerus1 = {'name': 'g_humerus1',
              'muscles': ['humerus1'],
              'n_II_interneurons': 196
}

g_humerus2 = {'name': 'g_humerus2',
              'muscles': ['humerus2'],
              'n_II_interneurons': 196
}

g_radius1 = {'name': 'g_radius1',
             'muscles': ['radius1'],
             'n_II_interneurons': 196
}

g_radius2 = {'name': 'g_radius2',
             'muscles': ['radius2'],
             'n_II_interneurons': 196
}

g_foot1 = {'name': 'g_foot1',
           'muscles': ['foot1'],
           'n_II_interneurons': 196
}

g_foot2 = {'name': 'g_foot2',
           'muscles': ['foot2'],
           'n_II_interneurons': 196
}


flex_extend_humerus = {'g1': {'group': 'g_humerus1', 'n_Ia_interneurons': 196},
                       'g2': {'group': 'g_humerus2', 'n_Ia_interneurons': 196}}

flex_extend_radius = {'g1': {'group': 'g_radius1', 'n_Ia_interneurons': 196},
                      'g2': {'group': 'g_radius2', 'n_Ia_interneurons': 196}}

flex_extend_foot = {'g1': {'group': 'g_foot1', 'n_Ia_interneurons': 196},
                    'g2': {'group': 'g_foot2', 'n_Ia_interneurons': 196}}

